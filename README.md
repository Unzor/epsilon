# Epsilon
A work-in-progress web framework that lets you run code before HTML is rendered.

# Usage
index.js:
```js
const express = require('express');
const epsilon = require('@diegab/epsilon');
const app = express();

epsilon.attach(app);
epsilon.render('public/main.epsln', '/');

app.listen(8080);
```
public/main.epsln:
```html
<!DOCTYPE HTML>
<html>
<head>
</head>
<body>
    <h1> Hello, World! </h1>
    <p> The date is: %{new Date()}, and this platform is %{process.platform}. </p>
    <p> Here's some color:
    <p id="color"></p>
    </p>
    <script epsilon>
        var colors = require('@colors/colors');
        var Convert = require('ansi-to-html');
        var convert = new Convert();

        document.querySelector('#color').innerHTML = convert.toHtml("CoLoRs!!".rainbow);
    </script>
    <script>
        alert('Some script that runs in the browser, not in the server.');
    </script>
</html>
```
Results in:

![Epsilion Result](screenshots/1.png)

# Documentation
- `epsilon.attach`:
    - Attaches Epsilon to an Express router.
    - Usage: 
    ```js
    const express = require("express");
    const epsilon = require("@diegab/epsilon");
    const app = express();
    epsilion.attach(app);
    ```
- `epsilon.render`:
    - Renders an Epsilion template to a path.
    - Usage: `epsilion.render("path/to/page.epsln", "/")`
- `epsilon.text_render`:
    - Renders an Epsilion template (in form of a string) to a path.
    - Usage: `epsilion.text_render("<h1> The date is: %{new Date()} </h1>", "/")`
- `epsilon.render_with_changes`:
    - Renders an Epsilion template to a path, but instead of having to render once, it will render the page every time it is loaded. 
    - Usage: `epsilion.text_render_with_changes("path/to/page.epsln", "/")`
- `epsilon.render_text_with_changes`:
    - Renders an Epsilion template (in form of a string) to a path, but instead of having to render once, it will render the page every time it is loaded. Not recommended as loading times will be slower.
    - Usage: `epsilion.render_text_with_changes("<h1> The date is: %{new Date()} </h1>", "/")`
- `epsilon.toHTML`:
    - Converts an Epsilion template (in form of a string) to HTML, but instead of having to render once, it will render the page every time it is loaded.  Not recommended as loading times will be slower.
    - Usage: `epsilion.toHTML("<h1> The date is: %{new Date()} </h1>")`
