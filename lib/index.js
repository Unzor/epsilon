let fs = require('fs');
function rangethrough(sequence, str) {
    var a2 = [];
    str.split(sequence[0]).forEach(function(e) {
        var h = e.split(sequence[1]);
        if (h.length == 1) {
            h = h[0];
        }
        a2.push(h)
    })
    var a3 = [];
    a2.forEach(function(e) {
        var type = typeof e;
        if (type == "object") {
            a3.push(sequence[0] + e[0] + sequence[1]);
            a3.push(e[1]);
        } else {
            a3.push(e);
        }
    })
    return a3;
}
let HTMLParser = require('node-html-parser');
this.app = null;
let attach = (app) => {
  this.app = app;
}

let render = (path, p) => {
    if (!this.app) throw new Error("Epsilon is not attached to a router yet") 
    var page = fs.readFileSync(path).toString();
    var fm1 = rangethrough(['%{', '}'], page);

    fm1.forEach(function(entry, index) {
        if (entry.startsWith('%{')) {
            fm1[index] = eval(entry.slice(2, -1))
        }
    })

    var document = HTMLParser.parse(fm1.join(''));

    let get_e_scripts = () => {
        var s = document.querySelectorAll('script');
        var n = [];
        s = s.map(x => x.rawAttrs.includes('epsilon') ? x : null)
        s.forEach((s) => s ? n.push(s) : null);
        return n
    }

    get_e_scripts().forEach(function(code, i) {
        eval(code.innerText);
        code.remove();
    });

    let r = document.toString();
    this.app.get(p, (req, res) => res.send(r));
}

let render_with_changes = (path, p) => {
    if (!this.app) throw new Error("Epsilon is not attached to a router yet") 
    this.app.get(p, (req, res) => {
          var page = fs.readFileSync(path).toString();
    var fm1 = rangethrough(['%{', '}'], page);

    fm1.forEach(function(entry, index) {
        if (entry.startsWith('%{')) {
            fm1[index] = eval(entry.slice(2, -1))
        }
    })

    var document = HTMLParser.parse(fm1.join(''));

    let get_e_scripts = () => {
        var s = document.querySelectorAll('script');
        var n = [];
        s = s.map(x => x.rawAttrs.includes('epsilon') ? x : null)
        s.forEach((s) => s ? n.push(s) : null);
        return n
    }

    get_e_scripts().forEach(function(code, i) {
        eval(code.innerText);
        code.remove();
    });

    let r = document.toString();
      res.send(r);
    });
}

let text_render_with_changes = (page, p) => {
    if (!this.app) throw new Error("Epsilon is not attached to a router yet") 
    this.app.get(p, (req, res) => {
    var fm1 = rangethrough(['%{', '}'], page);

    fm1.forEach(function(entry, index) {
        if (entry.startsWith('%{')) {
            fm1[index] = eval(entry.slice(2, -1))
        }
    })

    var document = HTMLParser.parse(fm1.join(''));

    let get_e_scripts = () => {
        var s = document.querySelectorAll('script');
        var n = [];
        s = s.map(x => x.rawAttrs.includes('epsilon') ? x : null)
        s.forEach((s) => s ? n.push(s) : null);
        return n
    }

    get_e_scripts().forEach(function(code, i) {
        eval(code.innerText);
        code.remove();
    });

    let r = document.toString();
      res.send(r);
    });
}

let text_render = (page, p) => {
    if (!this.app) throw new Error("Epsilon is not attached to a router yet") 
    var fm1 = rangethrough(['%{', '}'], page);

    fm1.forEach(function(entry, index) {
        if (entry.startsWith('%{')) {
            fm1[index] = eval(entry.slice(2, -1))
        }
    })

    var document = HTMLParser.parse(fm1.join(''));

    let get_e_scripts = () => {
        var s = document.querySelectorAll('script');
        var n = [];
        s = s.map(x => x.rawAttrs.includes('epsilon') ? x : null)
        s.forEach((s) => s ? n.push(s) : null);
        return n
    }

    get_e_scripts().forEach(function(code, i) {
        eval(code.innerText);
        code.remove();
    });

    let r = document.toString();
    this.app.get(p, (req, res) => res.send(r));
}

let toHTML = (page, p) => {
    var fm1 = rangethrough(['%{', '}'], page);

    fm1.forEach(function(entry, index) {
        if (entry.startsWith('%{')) {
            fm1[index] = eval(entry.slice(2, -1))
        }
    })

    var document = HTMLParser.parse(fm1.join(''));

    let get_e_scripts = () => {
        var s = document.querySelectorAll('script');
        var n = [];
        s = s.map(x => x.rawAttrs.includes('epsilon') ? x : null)
        s.forEach((s) => s ? n.push(s) : null);
        return n
    }

    get_e_scripts().forEach(function(code, i) {
        eval(code.innerText);
        code.remove();
    });

    let r = document.toString();
    return r;
}

module.exports = {
    attach,
    render,
    text_render,
    toHTML,
    render_with_changes,
    text_render_with_changes
};