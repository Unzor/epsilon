/*!
 * epsilon
 * Copyright(c) Unzor
 * MIT Licensed
 */

'use strict';

module.exports = require('./lib');
